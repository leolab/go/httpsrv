module gitlab.com/leolab/go/httpsrv

go 1.17

require (
	gitlab.com/leolab/go/errs v0.1.1
	gitlab.com/leolab/go/session v0.2.2
)

require gitlab.com/leolab/go/replacer v1.1.1 // indirect
