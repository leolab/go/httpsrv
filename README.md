# Сервер с поддержкой маршрутов и сессий

## SrvRec
  * Req *Request
  * Rsp *Response
  * Sess *Session
  * Data map[string]interface{}

## Использование
```go
package main

import (

)

func handle(sr *httpsrv.Rec) *httpsrv.Rec {
  return sr.SetContent([]byte("Response body"))
}

func handleVar(sr *httpsrv.Rec) *httpsrv.Rec {
  return sr.SetContent([]byte("Response with VAR value: "+sr.Req.Vars["var"]))
}

func main(){
    lst,err:=net.Listen("tcp",":8080")
    if err!=nil { panic(err) }
    srv := server.NewServer(lst)

    srv.Static("/assets/",files) //Обработка статических файлов
    srv.AddRoute("index",handle) // :8080/
    srv.AddRoute("path/$var",handleVar) // :8080/path/*
}

```

Обрабатываются запросы:
  * localhost:8080/assets/*
  * localhost:8080/path/*
  * localhost:8080/index
  * localhost:8080/
