package httpsrv

func NotFound(sr *Rec) *Rec {
	sr._content = []byte("Route not found: " + sr.Req.Lnk)
	sr.Rsp.code = 404
	return sr
}

func NoHandler(sr *Rec) *Rec {
	sr._content = []byte("No route handler: " + sr.Req.Lnk)
	sr.Rsp.code = 500
	return sr
}
