package httpsrv

import "gitlab.com/leolab/go/errs"

const (
	ErrServerError        errs.ErrCode = "ErrServerError"
	ErrReadBodyError      errs.ErrCode = "ErrReadBodyError"
	ErrHeadersAlreadySent errs.ErrCode = "ErrHeadersAlreadySent"
)
