package httpsrv

import (
	"fmt"
	"strings"
)

type HandleFunc func(sr *Rec) *Rec

type Route struct {
	value   string
	next    map[string]*Route
	handler HandleFunc
}

func NewRoute() *Route {
	r := &Route{
		next:    make(map[string]*Route),
		handler: nil,
	}
	return r
}

func (r *Route) Add(path string, h HandleFunc) {
	if path[len(path)-1] == '/' {
		path = path + "index"
	}
	pa := strings.Split(path, "/")
	r._append(pa, h)
}

func (r *Route) _append(pa []string, h HandleFunc) {
	f := pa[0]
	v := ""
	pa = pa[1:]
	if f[0] == '$' {
		v = f[1:]
		f = "$"
	}
	tr, ok := r.next[f]
	if !ok {
		tr = NewRoute()
		r.next[f] = tr
	}
	if f == "$" {
		tr.value = v
	}
	if len(pa) > 0 {
		tr._append(pa, h)
	} else {
		tr.handler = h
	}
}

func (r *Route) Do(sr *Rec) *Rec {
	f := sr.Next()
	if f != "" {
		if nr, ok := r.next[f]; ok {
			return nr.Do(sr)
		}
		if nr, ok := r.next["$"]; ok {
			fmt.Println("Variable: " + nr.value + " = " + f)
			sr.Req.Vars[nr.value] = f
			return nr.Do(sr)
		}
		return NotFound(sr)
	} else {
		if r.handler != nil {
			return r.handler(sr)
		}
		return NoHandler(sr)
	}
}
