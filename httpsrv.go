package httpsrv

import (
	"encoding/json"
	"fmt"
	"io/fs"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
	"sync"

	"gitlab.com/leolab/go/session"

	"gitlab.com/leolab/go/errs"
)

var Dbg bool = false

type Server struct {
	hande  HandleFunc
	routes *Route

	_lst net.Listener
	_mux *http.ServeMux
	_srv *http.Server

	_ses *session.Store

	_wg   *sync.WaitGroup
	_proc chan interface{}
}

type Handler func(sr *Rec) *Rec

func NewServer(lst net.Listener) (srv *Server, err *errs.Err) {
	srv = &Server{
		routes: NewRoute(),
		hande:  nil,
		_lst:   lst,
		_mux:   http.NewServeMux(),
		_srv:   &http.Server{},
		_proc:  make(chan interface{}),
		_ses:   nil,
		_wg:    &sync.WaitGroup{},
	}
	return srv, nil
}

func (s *Server) Init(cfg *session.Config) *errs.Err {
	s._ses = session.NewStore(cfg)
	return nil
}

//Add static route for path
func (s *Server) Static(path string, fs fs.FS) {
	s._mux.Handle(path, http.FileServer(http.FS(fs)))
}

//Add route
func (s *Server) AddRoute(path string, fn HandleFunc) {
	s.routes.Add(path, fn)
}

//Default handler, called when no one other hired
func (s *Server) DefaultHandle(fn HandleFunc) {
	s.hande = fn
}

func (s *Server) Start() {
	s._mux.HandleFunc("/", s.reqHandler)
	s._wg.Add(1)
	go s._exec()
}

func (s *Server) Stop() {
	close(s._proc)
	s._wg.Wait()
}

func (s *Server) _exec() {
	defer s._wg.Done()
	s._srv.Handler = s._mux
	go func() {
		if e := s._srv.Serve(s._lst); e != nil {
			errs.RaiseError(ErrServerError, e.Error())
		}
	}()
	<-s._proc
}

func (s *Server) reqHandler(w http.ResponseWriter, r *http.Request) {
	b, e := ioutil.ReadAll(r.Body)
	if e != nil {
		errs.RaiseError(ErrReadBodyError, e.Error())
		b = make([]byte, 0)
	}
	if r.URL.Path[len(r.URL.Path)-1] == '/' {
		r.URL.Path = r.URL.Path + "index"
	}
	fa := strings.Split(strings.TrimLeft(r.URL.Path, "/"), "/")
	if len(fa) == 0 {
		fa = append(fa, "index")
	}
	sr := &Rec{
		Req: &Request{
			R:    r,
			Body: b,
			F:    fa[0],
			FA:   fa[1:],
			Lnk:  strings.Join(fa, "/"),
			Vars: make(map[string]string),
		},
		Rsp: &Response{
			W:       w,
			headers: make(map[string][]string),
			code:    200,
		},
		Data: make(map[string]interface{}),
		Sess: s._ses.Get(w, r),

		_content: make([]byte, 0),
		_sent:    false,
	}
	if Dbg {
		fmt.Println("Request: " + sr.Req.Lnk)
		fmt.Println("Body: \n" + string(sr.Req.Body) + "\n")
	}

	if e := json.Unmarshal(b, &sr.Req.Data); e != nil {
		//No JSON data in body, or wrong
		//sr.Data = make(map[string]interface{})
		sr.Req.Data = nil
	}
	q := r.URL.Query()
	for k, v := range q {
		sr.Req.Vars[k] = strings.Join(v, ",")
	}
	rsp := s.routes.Do(sr)
	if rsp == nil {
		w.WriteHeader(500)
		w.Write([]byte(errs.RaiseError(ErrServerError, "NIL response").Error()))
		return
	}
	if !rsp._sent {
		rsp.Sent()
	}
}
