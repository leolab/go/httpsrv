package httpsrv

import (
	"net/http"

	"gitlab.com/leolab/go/session"
)

type Rec struct {
	Req  *Request
	Rsp  *Response
	Sess *session.Rec
	Data map[string]interface{}

	_content []byte
	_sent    bool
}

type Request struct {
	R        *http.Request
	Body     []byte
	Data     map[string]interface{}
	Lnk      string
	F        string
	FA       []string
	IsMobile bool
	Vars     map[string]string
}

func (s *Rec) Next() string {
	r := s.Req.F
	if len(s.Req.FA) > 0 {
		s.Req.F = s.Req.FA[0]
		s.Req.FA = s.Req.FA[1:]
	} else {
		s.Req.F = ""
	}
	return r
}
