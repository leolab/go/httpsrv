package httpsrv

import (
	"fmt"
	"net/http"

	"gitlab.com/leolab/go/errs"
)

//var _ Response = (*SrvRsp)(nil)

type Response struct {
	W http.ResponseWriter

	headers map[string][]string
	code    int
}

/*
func NewSrvRsp(code int, headers map[string][]string, content []byte) *Rec {
	rsp := &SrvRsp{
		code:    code,
		headers: headers,
		content: content,
	}
	if rsp.headers == nil {
		rsp.headers = map[string][]string{}
	}
	return rsp
}
*/

//Добавить HTTP-заголовок
func (r *Rec) HeaderAdd(key, val string) *Rec {
	if _, ok := r.Rsp.headers[key]; !ok {
		r.Rsp.headers[key] = make([]string, 0)
	}
	r.Rsp.headers[key] = append(r.Rsp.headers[key], val)
	return r
}

//Установить или изменить HTTP-Заголово
func (r *Rec) HeaderSet(key, val string) *Rec {
	r.Rsp.headers[key] = []string{val}
	return r
}

//Удалить HTTP-Заголовок
func (r *Rec) HeaderDel(key string) *Rec {
	delete(r.Rsp.headers, key)
	return r
}

//Указать перенаправление.
//Код ответа устанавливается в 303
func (r *Rec) Redir(url string) *Rec {
	r.HeaderSet("Location", url)
	r.Rsp.code = 303
	return r
}

//Установить код ответа
func (r *Rec) SetCode(code int) *Rec {
	r.Rsp.code = code
	return r
}

//Отправить данные клиенту
func (r *Rec) Sent() {
	if r._sent {
		//Error, data and headers already sended
		//Prevent for dupe
		errs.RaiseError(ErrHeadersAlreadySent, "Headers already sent")
		return
	}
	for k, va := range r.Rsp.headers {
		for _, v := range va {
			r.Rsp.W.Header().Add(k, v)
		}
	}
	r.HeaderSet("Content-Length", fmt.Sprintf("%d", len(r._content)))
	r.Rsp.W.WriteHeader(r.Rsp.code)
	if len(r._content) > 0 {
		r.Rsp.W.Write(r._content)
	}
	r._sent = true
}

//Установить тело ответа
func (r *Rec) SetContent(content []byte) *Rec {
	r._content = content
	return r
}
